(* map_lib.v *)

(* This file introduces a simple API exposing operations to manipulate maps. *)

From hazel.program_logic Require Import reasoning_rules.


(* ========================================================================== *)
(** * Map Interface. *)

Class MapLib Σ `{!irisGS eff_lang Σ} := {
  make_map   : val;
  map_lookup : val;
  map_insert : val;
  map_delete : val;

  is_map : val → gmap val val → iProp Σ;

  make_map_spec E Ψ1 Ψ2 :
    ⊢ EWP make_map #() @ E <| Ψ1 |> {| Ψ2 |} {{ m, is_map m ∅ }};

  map_lookup_spec E Ψ1 Ψ2 M (m a : val) :
    is_map m M -∗
      EWP map_lookup m a @ E <| Ψ1 |> {| Ψ2 |} {{ y,
        is_map m M  ∗
        ⌜ (match M !! a with
           | Some b' => y = SOMEV b'
           | None    => y = NONEV
           end) ⌝
      }};

  map_insert_spec E Ψ1 Ψ2 M (m a b : val) :
    is_map m M -∗
      EWP map_insert m a b @ E <| Ψ1 |> {| Ψ2 |} {{ _,
        is_map m (<[a := b]> M) }};

  map_delete_spec E Ψ1 Ψ2 M (m a : val) :
    is_map m M -∗
      EWP map_delete m a @ E <| Ψ1 |> {| Ψ2 |} {{ _,
        is_map m (delete a M) }};
}.
